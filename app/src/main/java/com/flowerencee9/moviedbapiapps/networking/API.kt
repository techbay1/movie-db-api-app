package com.flowerencee9.moviedbapiapps.networking

import com.flowerencee9.moviedbapiapps.model.response.GenreResponse
import com.flowerencee9.moviedbapiapps.model.response.ListMovieResponse
import com.flowerencee9.moviedbapiapps.model.response.ListReviewResponse
import com.flowerencee9.moviedbapiapps.model.response.MovieDetailResponse
import com.flowerencee9.moviedbapiapps.support.Constants.ENDPOINT.Companion.GET_DETAIL_MOVIE
import com.flowerencee9.moviedbapiapps.support.Constants.ENDPOINT.Companion.GET_GENRE_LIST
import com.flowerencee9.moviedbapiapps.support.Constants.ENDPOINT.Companion.GET_MOVIES_LIST
import com.flowerencee9.moviedbapiapps.support.Constants.ENDPOINT.Companion.GET_REVIEW
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

class API {

    interface MOVIESERVICE {

        @GET(GET_GENRE_LIST)
        fun getGenreList(): Call<GenreResponse>

        @GET(GET_MOVIES_LIST)
        suspend fun getMovie(
            @Query("page") page: Int,
            @Query("with_genres") genre: String
        ): ListMovieResponse

        @GET(GET_REVIEW)
        suspend fun getReview(
            @Path("movie_id") movieId: Int,
            @Query("page") page: Int
        ): ListReviewResponse

        @GET(GET_DETAIL_MOVIE)
        fun getDetailMovie(
            @Path("movie_id") movieId: Int,
            @Query("append_to_response") videos: String
        ): Call<MovieDetailResponse>
    }
}