package com.flowerencee9.moviedbapiapps.networking

import android.content.Context
import com.flowerencee9.moviedbapiapps.BuildConfig

class Services(private val context: Context) {
    val movieService: API.MOVIESERVICE
    get() = APIClient.getClient(BuildConfig.BASE_URL, context).create(API.MOVIESERVICE::class.java)
}