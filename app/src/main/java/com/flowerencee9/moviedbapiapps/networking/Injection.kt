package com.flowerencee9.moviedbapiapps.networking

import android.content.Context
import com.flowerencee9.moviedbapiapps.model.repository.DetailRepository
import com.flowerencee9.moviedbapiapps.model.repository.MovieRepository

object Injection {
    fun provideMovie(context: Context): MovieRepository {
        val apiService = Services(context).movieService
        return MovieRepository(apiService)
    }

    fun provideReview(context: Context): DetailRepository {
        val service = Services(context).movieService
        return DetailRepository(service)
    }
}