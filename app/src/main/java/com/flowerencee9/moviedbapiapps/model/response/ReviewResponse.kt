package com.flowerencee9.moviedbapiapps.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ReviewResponse(
    @SerializedName("author")
    var author: String = "",
    @SerializedName("author_details")
    var authorDetails: AuthorDetails = AuthorDetails(),
    @SerializedName("content")
    var content: String = "",
    @SerializedName("created_at")
    var createdAt: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("updated_at")
    var updatedAt: String = "",
    @SerializedName("url")
    var url: String = ""
) : Parcelable