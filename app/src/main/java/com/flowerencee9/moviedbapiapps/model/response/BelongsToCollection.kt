package com.flowerencee9.moviedbapiapps.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class BelongsToCollection(
    @SerializedName("backdrop_path")
    var backdropPath: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("poster_path")
    var posterPath: String = ""
) : Parcelable