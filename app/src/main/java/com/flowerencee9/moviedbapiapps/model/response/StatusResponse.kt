package com.flowerencee9.moviedbapiapps.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class StatusResponse(
    @SerializedName("status_code")
    var statusCode: Int = 0,
    @SerializedName("status_message")
    var statusMessage: String = "",
    @SerializedName("success")
    var success: Boolean = false
) : Parcelable