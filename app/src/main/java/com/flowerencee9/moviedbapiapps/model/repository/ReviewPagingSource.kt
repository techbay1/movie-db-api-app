package com.flowerencee9.moviedbapiapps.model.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.flowerencee9.moviedbapiapps.model.response.ReviewResponse
import com.flowerencee9.moviedbapiapps.networking.API

class ReviewPagingSource(
    private val service: API.MOVIESERVICE,
    private val movieId: Int
) : PagingSource<Int, ReviewResponse>() {
    private companion object {
        const val INITIAL_PAGE_INDEX = 1
    }

    override fun getRefreshKey(state: PagingState<Int, ReviewResponse>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ReviewResponse> {
        return try {
            val position = params.key ?: INITIAL_PAGE_INDEX
            val responseData = service.getReview(movieId, position).reviewResponses
            LoadResult.Page(
                data = responseData,
                prevKey = if (position == INITIAL_PAGE_INDEX) null else position - 1,
                nextKey = if (responseData.isEmpty()) null else position + 1
            )
        } catch (exception: Exception) {
            return LoadResult.Error(exception)
        }
    }
}