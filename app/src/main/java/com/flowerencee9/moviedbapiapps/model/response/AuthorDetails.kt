package com.flowerencee9.moviedbapiapps.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class AuthorDetails(
    @SerializedName("avatar_path")
    var avatarPath: String? = null,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("rating")
    var rating: Double? = null,
    @SerializedName("username")
    var username: String = ""
) : Parcelable