package com.flowerencee9.moviedbapiapps.model.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.flowerencee9.moviedbapiapps.model.response.Genre
import com.flowerencee9.moviedbapiapps.model.response.GenreResponse
import com.flowerencee9.moviedbapiapps.model.response.MoviesResultResponse
import com.flowerencee9.moviedbapiapps.model.response.StatusResponse
import com.flowerencee9.moviedbapiapps.networking.API
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieRepository(private val service: API.MOVIESERVICE) {
    fun getMovie(genreId: String): LiveData<PagingData<MoviesResultResponse>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20
            ),
            pagingSourceFactory = {
                MoviePagingSource(service, genreId)
            }
        ).liveData
    }

    private val _statusResponse: MutableLiveData<StatusResponse> = MutableLiveData()
    val statusResponse: LiveData<StatusResponse> get() = _statusResponse

    private val _loadingStates: MutableLiveData<Boolean> = MutableLiveData()
    val loadingStates: LiveData<Boolean> get() = _loadingStates

    private var _genreList: MutableLiveData<ArrayList<Genre>> = MutableLiveData()
    val genreList: LiveData<ArrayList<Genre>> get() = _genreList

    fun getGenres() {
        val callback = object : Callback<GenreResponse> {
            override fun onResponse(call: Call<GenreResponse>, response: Response<GenreResponse>) {
                if (response.isSuccessful) {
                    val genreResponse = generateLabel(response.body()?.genres)
                    _genreList.value = genreResponse

                } else {
                    _statusResponse.value =
                        StatusResponse(response.code(), response.message(), false)
                }
                _loadingStates.value = false
            }

            override fun onFailure(call: Call<GenreResponse>, t: Throwable) {
                _statusResponse.value = StatusResponse(0, t.message.toString(), false)
                _loadingStates.value = false
            }

        }
        _loadingStates.value = true
        service.getGenreList().enqueue(callback)
    }

    private fun generateLabel(genres: ArrayList<Genre>?): ArrayList<Genre> {
        val labelList = ArrayList<Genre>()
        genres?.forEach {
            labelList.add(
                Genre(
                    name = if (it.name.isNotEmpty()) it.name.take(1).uppercase() else ""
                )
            )
        }
        labelList.distinctBy { it.name }.forEach {
            genres?.add(it)
        }
        return genres?.sortedWith(compareBy { it.name })?.toCollection(ArrayList()) ?: ArrayList()
    }

}