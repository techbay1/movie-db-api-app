package com.flowerencee9.moviedbapiapps.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class SpokenLanguage(
    @SerializedName("english_name")
    var englishName: String = "",
    @SerializedName("iso_639_1")
    var iso6391: String = "",
    @SerializedName("name")
    var name: String = ""
) : Parcelable