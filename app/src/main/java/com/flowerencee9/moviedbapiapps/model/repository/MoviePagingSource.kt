package com.flowerencee9.moviedbapiapps.model.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.flowerencee9.moviedbapiapps.model.response.MoviesResultResponse
import com.flowerencee9.moviedbapiapps.networking.API

class MoviePagingSource(
    private val service: API.MOVIESERVICE,
    private val genreId: String
) : PagingSource<Int, MoviesResultResponse>() {
    private companion object {
        const val INITIAL_PAGE_INDEX = 1
    }

    override fun getRefreshKey(state: PagingState<Int, MoviesResultResponse>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MoviesResultResponse> {
        return try {
            val position = params.key ?: INITIAL_PAGE_INDEX
            val responseData = service.getMovie(position, genreId).resultResponses
            LoadResult.Page(
                data = responseData,
                prevKey = if (position == INITIAL_PAGE_INDEX) null else position - 1,
                nextKey = if (responseData.isEmpty()) null else position + 1
            )
        } catch (exception: Exception) {
            return LoadResult.Error(exception)
        }
    }

}