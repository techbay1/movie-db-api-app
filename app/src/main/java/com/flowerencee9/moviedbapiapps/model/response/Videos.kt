package com.flowerencee9.moviedbapiapps.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Videos(
    @SerializedName("results")
    var videoResults: List<VideoResult> = listOf()
) : Parcelable