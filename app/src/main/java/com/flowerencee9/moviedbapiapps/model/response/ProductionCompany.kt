package com.flowerencee9.moviedbapiapps.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductionCompany(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("logo_path")
    var logoPath: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("origin_country")
    var originCountry: String = ""
) : Parcelable