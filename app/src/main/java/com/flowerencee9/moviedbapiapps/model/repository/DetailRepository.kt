package com.flowerencee9.moviedbapiapps.model.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.flowerencee9.moviedbapiapps.model.response.MovieDetailResponse
import com.flowerencee9.moviedbapiapps.model.response.ReviewResponse
import com.flowerencee9.moviedbapiapps.model.response.StatusResponse
import com.flowerencee9.moviedbapiapps.networking.API
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailRepository(
    private val service: API.MOVIESERVICE
) {

    private val _statusResponse: MutableLiveData<StatusResponse> = MutableLiveData()
    val statusResponse: LiveData<StatusResponse> get() = _statusResponse

    private val _loadingStates: MutableLiveData<Boolean> = MutableLiveData()
    val loadingStates: LiveData<Boolean> get() = _loadingStates

    private var _detailMovie: MutableLiveData<MovieDetailResponse> = MutableLiveData()
    val detailMovie: LiveData<MovieDetailResponse> get() = _detailMovie

    fun getReview(movieId: Int): LiveData<PagingData<ReviewResponse>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10
            ),
            pagingSourceFactory = {
                ReviewPagingSource(service, movieId)
            }
        ).liveData
    }

    fun getDetailMovie(movieId: Int) {
        val callback = object : Callback<MovieDetailResponse> {
            override fun onResponse(
                call: Call<MovieDetailResponse>,
                response: Response<MovieDetailResponse>
            ) {
                Log.d("DetailRepository", "calling success ${response.isSuccessful}")
                Log.d("DetailRepository", "calling success ${response.message()}")
                if (response.isSuccessful) {
                    _detailMovie.value = response.body()
                    _statusResponse.value =
                        StatusResponse(response.code(), response.message(), true)
                } else {
                    _statusResponse.value =
                        StatusResponse(response.code(), response.message(), false)
                }
                _loadingStates.value = false
            }

            override fun onFailure(call: Call<MovieDetailResponse>, t: Throwable) {
                _statusResponse.value = StatusResponse(0, t.message.toString(), false)
                _loadingStates.value = false
            }

        }
        _loadingStates.value = true
        service.getDetailMovie(movieId, "videos").enqueue(callback)
    }
}