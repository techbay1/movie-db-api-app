package com.flowerencee9.moviedbapiapps.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class VideoResult(
    @SerializedName("id")
    var id: String = "",
    @SerializedName("iso_3166_1")
    var iso31661: String = "",
    @SerializedName("iso_639_1")
    var iso6391: String = "",
    @SerializedName("key")
    var key: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("official")
    var official: Boolean = false,
    @SerializedName("published_at")
    var publishedAt: String = "",
    @SerializedName("site")
    var site: String = "",
    @SerializedName("size")
    var size: Int = 0,
    @SerializedName("type")
    var type: String = ""
) : Parcelable