package com.flowerencee9.moviedbapiapps.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ListMovieResponse(
    @SerializedName("page")
    var page: Int = 0,
    @SerializedName("results")
    var resultResponses: List<MoviesResultResponse> = listOf(),
    @SerializedName("total_pages")
    var totalPages: Int = 0,
    @SerializedName("total_results")
    var totalResults: Int = 0
) : Parcelable