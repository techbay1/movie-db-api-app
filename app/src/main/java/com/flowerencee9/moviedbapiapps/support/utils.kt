package com.flowerencee9.moviedbapiapps.support

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.flowerencee9.moviedbapiapps.R
import com.flowerencee9.moviedbapiapps.databinding.LayoutPopupBinding
import java.text.SimpleDateFormat
import java.util.*

fun String.reformatDateFromString(
    newFormat: String = "yyyy MMM dd | HH:mm",
    oldFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
): String {
    return try {
        var format = SimpleDateFormat(oldFormat, Locale.getDefault())
        val newDate = format.parse(this)
        format = SimpleDateFormat(newFormat, Locale.getDefault())
        newDate?.let { format.format(it) }.toString()
    } catch (e: Exception) {
        e.printStackTrace()
        this
    }
}

fun View.toVisible() {
    this.visibility = View.VISIBLE
}

fun View.toHide() {
    this.visibility = View.GONE
}

fun Activity.showPopupSelection(
    illustration: Drawable? = null,
    title: String? = null,
    message: String? = null,
    primary: String? = null,
    secondary: String? = null,
    root: ViewGroup? = null,
    clickListener: PopUpCallback
) {
    val dialog = Dialog(this, R.style.DialogSlideAnimFullWidth)
    val binding = LayoutPopupBinding.bind(
        layoutInflater.inflate(
            R.layout.layout_popup,
            root,
            false
        )
    )
    dialog.apply {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(binding.root)
        setCancelable(false)
    }
    with(binding) {
        title?.let { tvTitle.text = it }
        message?.let { tvBody.text = it }
        primary?.let { btnPositive.text = it }
        secondary?.let { btnSecondary.text = it }
        illustration?.let { ivIllustration.setImageDrawable(it) }
        btnPositive.setOnClickListener {
            dialog.dismiss()
            clickListener.onPrimary()
        }
        btnSecondary.setOnClickListener {
            dialog.dismiss()
            clickListener.onSecondary()
        }

    }
    if (!isFinishing) dialog.show()
}

interface PopUpCallback {
    fun onPrimary()
    fun onSecondary()
}