package com.flowerencee9.moviedbapiapps.support

import com.flowerencee9.moviedbapiapps.BuildConfig

object Constants {
    interface ENDPOINT {
        companion object {
            const val GET_GENRE_LIST = "genre/movie/list?" + BuildConfig.API_KEY
            const val GET_MOVIES_LIST = "discover/movie?" + BuildConfig.API_KEY
            const val GET_REVIEW = "movie/{movie_id}/reviews?" + BuildConfig.API_KEY
            const val GET_DETAIL_MOVIE = "movie/{movie_id}?" + BuildConfig.API_KEY
        }
    }
    interface CONSTANTS {
        companion object {
            const val IMAGE_ORIGINAL = BuildConfig.IMAGE_URL + "original/"
            const val IMAGE_BACKDROP_QUALITY = BuildConfig.IMAGE_URL + "w300/"
            const val YOUTUBE_THUMBNAIL_QUALITY = "/0.jpg"
        }
    }
}