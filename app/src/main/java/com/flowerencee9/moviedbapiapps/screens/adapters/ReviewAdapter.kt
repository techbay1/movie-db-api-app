package com.flowerencee9.moviedbapiapps.screens.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.flowerencee9.moviedbapiapps.R
import com.flowerencee9.moviedbapiapps.databinding.LayoutItemReviewBinding
import com.flowerencee9.moviedbapiapps.model.response.ReviewResponse
import com.flowerencee9.moviedbapiapps.support.reformatDateFromString

class ReviewAdapter(
    private val movieListener: (ReviewResponse) -> Unit
) : PagingDataAdapter<ReviewResponse, ReviewAdapter.ViewHolder>(DIFF_CALLBACK) {
    private var _snapshot: MutableLiveData<List<ReviewResponse>> = MutableLiveData()
    val snapshot: LiveData<List<ReviewResponse>> get() = _snapshot

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ReviewResponse>() {
            override fun areItemsTheSame(
                oldItem: ReviewResponse,
                newItem: ReviewResponse
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: ReviewResponse,
                newItem: ReviewResponse
            ): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }

    inner class ViewHolder(
        private val binding: LayoutItemReviewBinding,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ReviewResponse) {
            with(binding) {
                tvAuthor.text = item.author
                tvReview.text = item.content
                val createdDate =
                    "${context.getString(R.string.date_created)} ${item.createdAt.reformatDateFromString()}"
                tvPublishDate.text = createdDate
                root.setOnClickListener {
                    movieListener(item)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            LayoutItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = getItem(position)
        val listData = snapshot().items
        _snapshot.value = listData
        if (data != null) holder.bind(data)
    }
}