package com.flowerencee9.moviedbapiapps.screens.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.flowerencee9.moviedbapiapps.R
import com.flowerencee9.moviedbapiapps.databinding.LayoutItemGenreBinding
import com.flowerencee9.moviedbapiapps.model.response.Genre
import com.flowerencee9.moviedbapiapps.support.toHide
import com.flowerencee9.moviedbapiapps.support.toVisible

class GenreAdapter(
    private val clickListener: (Genre) -> Unit
) : RecyclerView.Adapter<GenreAdapter.ViewHolder>() {
    private val listData = ArrayList<Genre>()

    inner class ViewHolder(itemVIew: View) : RecyclerView.ViewHolder(itemVIew) {
        fun bind(item: Genre) = with(itemView) {
            val binding = LayoutItemGenreBinding.bind(itemView)
            with(binding) {
                when (item.id == null) {
                    true -> {
                        containerLabel.toVisible()
                        containerContent.toHide()
                        lblAlphabet.text = item.name
                    }
                    else -> {
                        containerLabel.toHide()
                        containerContent.toVisible()
                        tvGenre.text = item.name
                        root.setOnClickListener {
                            clickListener(item)
                        }
                    }
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            (LayoutInflater.from(parent.context).inflate(R.layout.layout_item_genre, parent, false))
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listData[position])
    }

    override fun getItemCount(): Int = listData.size

    fun setData(list: ArrayList<Genre>) {
        val callbackDiff = GenreCallback(listData, list)
        val resultDiff = DiffUtil.calculateDiff(callbackDiff)
        listData.clear()
        listData.addAll(list)
        resultDiff.dispatchUpdatesTo(this)
    }

    inner class GenreCallback(
        private val oldList: ArrayList<Genre>,
        private val newList: ArrayList<Genre>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] === newList[newItemPosition]
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val (_, value0, nameOld) = oldList[oldItemPosition]
            val (_, value1, nameNew) = newList[newItemPosition]
            return nameOld == nameNew && value0 == value1
        }

        @Nullable
        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            return super.getChangePayload(oldItemPosition, newItemPosition)
        }

    }
}