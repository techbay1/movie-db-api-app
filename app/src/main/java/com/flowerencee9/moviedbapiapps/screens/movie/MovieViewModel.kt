package com.flowerencee9.moviedbapiapps.screens.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.flowerencee9.moviedbapiapps.model.repository.MovieRepository
import com.flowerencee9.moviedbapiapps.model.response.MoviesResultResponse

class MovieViewModel(repository: MovieRepository, genreId: String) : ViewModel() {
    val movies: LiveData<PagingData<MoviesResultResponse>> = repository.getMovie(genreId).cachedIn(viewModelScope)
}