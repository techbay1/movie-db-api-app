package com.flowerencee9.moviedbapiapps.screens.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.flowerencee9.moviedbapiapps.R
import com.flowerencee9.moviedbapiapps.databinding.LayoutItemMovieBinding
import com.flowerencee9.moviedbapiapps.model.response.MoviesResultResponse
import com.flowerencee9.moviedbapiapps.support.Constants.CONSTANTS.Companion.IMAGE_BACKDROP_QUALITY
import com.flowerencee9.moviedbapiapps.support.reformatDateFromString
import com.flowerencee9.moviedbapiapps.support.toHide

class MovieAdapter(
    private val movieListener: (MoviesResultResponse) -> Unit
) : PagingDataAdapter<MoviesResultResponse, MovieAdapter.ViewHolder>(DIFF_CALLBACK) {
    private var _snapshot: MutableLiveData<List<MoviesResultResponse>> = MutableLiveData()
    val snapshot: LiveData<List<MoviesResultResponse>> get() = _snapshot

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<MoviesResultResponse>() {
            override fun areItemsTheSame(
                oldItem: MoviesResultResponse,
                newItem: MoviesResultResponse
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: MoviesResultResponse,
                newItem: MoviesResultResponse
            ): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }

    inner class ViewHolder(
        private val binding: LayoutItemMovieBinding,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MoviesResultResponse) {
            with(binding) {
                tvTitle.text = item.title
                if (item.backdropPath != null) {
                    val imgUrl = IMAGE_BACKDROP_QUALITY + item.backdropPath
                    Glide.with(context).load(imgUrl).into(ivThumbnail)
                }
                if (item.releaseDate.isNotEmpty()) {
                    val datePublished =
                        "${context.getString(R.string.date_released)} ${
                            item.releaseDate.reformatDateFromString(
                                "yyyy MMM dd",
                                "yyyy-MM-dd"
                            )
                        }"
                    tvReleasedDate.text = datePublished
                } else tvReleasedDate.toHide()

                tvReleasedDate.text = item.releaseDate
                root.setOnClickListener {
                    movieListener(item)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            LayoutItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = getItem(position)
        val listData = snapshot().items
        _snapshot.value = listData
        if (data != null) holder.bind(data)
    }
}