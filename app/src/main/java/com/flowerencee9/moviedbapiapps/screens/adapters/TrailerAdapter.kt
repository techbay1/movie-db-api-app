package com.flowerencee9.moviedbapiapps.screens.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.flowerencee9.moviedbapiapps.BuildConfig
import com.flowerencee9.moviedbapiapps.R
import com.flowerencee9.moviedbapiapps.databinding.LayoutItemTrailerBinding
import com.flowerencee9.moviedbapiapps.model.response.VideoResult
import com.flowerencee9.moviedbapiapps.support.Constants.CONSTANTS.Companion.YOUTUBE_THUMBNAIL_QUALITY
import com.flowerencee9.moviedbapiapps.support.reformatDateFromString

class TrailerAdapter(
    private val clickListener: (VideoResult) -> Unit
) : RecyclerView.Adapter<TrailerAdapter.ViewHolder>() {

    private val listData = ArrayList<VideoResult>()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: VideoResult) = with(itemView) {
            val binding = LayoutItemTrailerBinding.bind(itemView)
            with(binding) {
                tvTitle.text = item.name
                val datePublished =
                    "${context.getString(R.string.date_published)} ${item.publishedAt.reformatDateFromString()}"
                tvPublishDate.text = datePublished
                val thumbnail =
                    BuildConfig.YOUTUBE_THUMBNAIL_URL + item.key + YOUTUBE_THUMBNAIL_QUALITY
                Glide.with(context).load(thumbnail).into(ivThumbnail)
                root.setOnClickListener {
                    clickListener(item)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            (LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_item_trailer, parent, false))
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listData[position])
    }

    override fun getItemCount(): Int = listData.size

    fun setData(list: ArrayList<VideoResult>) {
        list.apply {
            dropWhile { !it.site.equals("youtube", true) }
        }
        val callbackDiff = TrailerCallback(listData, list)
        val resultDiff = DiffUtil.calculateDiff(callbackDiff)
        listData.clear()
        listData.addAll(list)
        resultDiff.dispatchUpdatesTo(this)
    }

    inner class TrailerCallback(
        private val oldList: ArrayList<VideoResult>,
        private val newList: ArrayList<VideoResult>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] === newList[newItemPosition]
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val (_, value0, nameOld) = oldList[oldItemPosition]
            val (_, value1, nameNew) = newList[newItemPosition]
            return nameOld == nameNew && value0 == value1
        }

        @Nullable
        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            return super.getChangePayload(oldItemPosition, newItemPosition)
        }

    }

}