package com.flowerencee9.moviedbapiapps.screens.trailer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.flowerencee9.moviedbapiapps.BuildConfig
import com.flowerencee9.moviedbapiapps.databinding.ActivityYoutubePlayerBinding
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer

class TrailerActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {

    companion object {
        private val TAG = TrailerActivity::class.java.simpleName
        private const val EXTRA_VIDEO_ID = "EXTRA_YOUTUBE_ID"
        fun myIntent(context: Context, videoId: String) =
            Intent(context, TrailerActivity::class.java).apply {
                putExtra(EXTRA_VIDEO_ID, videoId)
            }
    }

    private val videoId: String by lazy {
        intent.getStringExtra(EXTRA_VIDEO_ID) ?: "dQw4w9WgXcQ"
    }

    private lateinit var binding: ActivityYoutubePlayerBinding
    override fun onCreate(p0: Bundle?) {
        super.onCreate(p0)
        binding = ActivityYoutubePlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupView()
    }

    private fun setupView() {
        binding.youtubePlayer.initialize(BuildConfig.GOOGLE_API_KEY, this)
    }

    override fun onInitializationSuccess(
        p0: YouTubePlayer.Provider?,
        p1: YouTubePlayer?,
        p2: Boolean
    ) {
        Log.d(TAG, "onInitializationSuccess: provider ${p0?.javaClass}")
        Log.d(TAG, "onInitializationSuccess: youTubePlayer ${p1?.javaClass}")
        Toast.makeText(this, "Initialized Youtube Player successfully", Toast.LENGTH_SHORT).show()

        p1?.setPlayerStateChangeListener(playerStateChangeListener)
        p1?.setPlaybackEventListener(playbackEventListener)

        if (!p2) {
            p1?.cueVideo(videoId)
        }
    }

    override fun onInitializationFailure(
        p0: YouTubePlayer.Provider?,
        p1: YouTubeInitializationResult?
    ) {
        val requestCode = 0

        if (p1?.isUserRecoverableError == true) {
            p1.getErrorDialog(this, requestCode).show()
        } else {
            val errorMessage = "There was an error initializing the YoutubePlayer ($p1)"
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
        }
    }

    private val playbackEventListener = object : YouTubePlayer.PlaybackEventListener {
        override fun onSeekTo(p0: Int) {
        }

        override fun onBuffering(p0: Boolean) {
        }

        override fun onPlaying() {
            Toast.makeText(this@TrailerActivity, "Playing", Toast.LENGTH_SHORT)
                .show()
        }

        override fun onStopped() {
            Log.d(TAG, "Stopped")
        }

        override fun onPaused() {
            Log.d(TAG, "Paused")
        }
    }

    private val playerStateChangeListener = object : YouTubePlayer.PlayerStateChangeListener {
        override fun onAdStarted() {
            Log.d(TAG, "Ad Clicked")
        }

        override fun onLoading() {
        }

        override fun onVideoStarted() {
            Log.d(TAG, "Video Started")
        }

        override fun onLoaded(p0: String?) {
        }

        override fun onVideoEnded() {
            Log.d(TAG, "Video ended")
        }

        override fun onError(p0: YouTubePlayer.ErrorReason?) {
        }
    }
}