package com.flowerencee9.moviedbapiapps.screens.detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.flowerencee9.moviedbapiapps.R
import com.flowerencee9.moviedbapiapps.databinding.ActivityDetailBinding
import com.flowerencee9.moviedbapiapps.model.response.MovieDetailResponse
import com.flowerencee9.moviedbapiapps.model.response.MoviesResultResponse
import com.flowerencee9.moviedbapiapps.model.response.ReviewResponse
import com.flowerencee9.moviedbapiapps.model.response.VideoResult
import com.flowerencee9.moviedbapiapps.networking.Injection
import com.flowerencee9.moviedbapiapps.screens.adapters.LoadingStateAdapter
import com.flowerencee9.moviedbapiapps.screens.adapters.ReviewAdapter
import com.flowerencee9.moviedbapiapps.screens.adapters.TrailerAdapter
import com.flowerencee9.moviedbapiapps.screens.trailer.TrailerActivity
import com.flowerencee9.moviedbapiapps.support.*
import com.flowerencee9.moviedbapiapps.support.Constants.CONSTANTS.Companion.IMAGE_ORIGINAL

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private lateinit var viewModel: DetailViewModel

    private val movie: MoviesResultResponse by lazy {
        intent.getParcelableExtra(EXTRA_MOVIE) ?: MoviesResultResponse()
    }

    private val reviewAdapter: ReviewAdapter by lazy {
        ReviewAdapter { reviewResponse ->
            reviewClicked(reviewResponse)
        }
    }

    private val trailerAdapter: TrailerAdapter by lazy {
        TrailerAdapter { videoResult ->
            videoClicked(videoResult)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        viewModel = DetailViewModel(Injection.provideReview(this), movie.id)
        setContentView(binding.root)
        setupView()
        initRequest()
    }

    private fun reviewClicked(review: ReviewResponse) {
        Log.d(TAG, "clicked review $review")
    }

    private fun videoClicked(video: VideoResult) {
        Log.d(TAG, "video clicked $video")
        startActivity(TrailerActivity.myIntent(this, video.key))
    }

    private fun setupView() {
        with(binding) {
            rvMain.apply {
                adapter = reviewAdapter.withLoadStateFooter(
                    footer = LoadingStateAdapter {
                        reviewAdapter.retry()
                    }
                )
                layoutManager = LinearLayoutManager(this@DetailActivity)
            }
            rvTrailer.apply {
                adapter = trailerAdapter
                layoutManager =
                    LinearLayoutManager(this@DetailActivity, LinearLayoutManager.HORIZONTAL, false)
            }
            trailerEmpty.tvMessage.setTextColor(
                ContextCompat.getColor(
                    this@DetailActivity,
                    R.color.night_fall
                )
            )
            trailerEmpty.imgIllustration.setImageDrawable(
                AppCompatResources.getDrawable(
                    this@DetailActivity,
                    R.drawable.ic_content_paste_off_nightfall
                )
            )
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.statusResponse.observe(this) {
            if (!it.success) {
                val selectionListener = object : PopUpCallback {
                    override fun onPrimary() {
                        initRequest()
                    }

                    override fun onSecondary() {
                        finish()
                    }

                }
                showPopupSelection(
                    null,
                    it.statusCode.toString(),
                    it.statusMessage,
                    getString(R.string.retry),
                    getString(R.string.close),
                    binding.root,
                    selectionListener
                )
            }
        }
    }

    private fun initRequest() {
        viewModel.review.observe(this) {
            reviewAdapter.submitData(lifecycle, it)
        }
        viewModel.getDetailMovie()
        viewModel.detailMovie.observe(this) {
            it?.let { insertDataToView(it) }
            Log.d(TAG, "movie detail data $it")
        }
        viewModel.loadingStates.observe(this) {
            Log.d(TAG, "loading states $it")
        }
        reviewAdapter.snapshot.observe(this) {
            Log.d(TAG, "review data $it")
            with(binding) {
                when (it.isNotEmpty()) {
                    true -> {
                        rvMain.toVisible()
                        containerReviewEmpty.toHide()
                    }
                    else -> {
                        rvMain.toHide()
                        containerReviewEmpty.toVisible()
                    }
                }
            }
        }
    }

    private fun insertDataToView(movie: MovieDetailResponse) {
        with(binding) {
            movie.let {
                val imgUrl = when {
                    it.posterPath.isNotEmpty() -> IMAGE_ORIGINAL + it.posterPath
                    it.backdropPath.isNotEmpty() -> IMAGE_ORIGINAL + it.backdropPath
                    else -> ""
                }
                when (imgUrl.isNotEmpty()) {
                    true -> Glide.with(this@DetailActivity).load(imgUrl).into(ivBanner)
                    else -> ivBanner.setImageDrawable(
                        AppCompatResources.getDrawable(
                            this@DetailActivity,
                            R.drawable.ic_launcher_foreground
                        )
                    )
                }

                tvTitle.text = it.title
                tvOverview.text = it.overview

                val releasedDate = when (it.status.equals(RELEASED, true)) {
                    true -> {
                        "${getString(R.string.date_released)} - ${
                            it.releaseDate.reformatDateFromString(
                                "yyyy MMM dd",
                                "yyyy-MM-dd"
                            )
                        }"
                    }
                    else -> ""
                }
                tvReleased.text = releasedDate
                when (it.videos.videoResults.isNotEmpty()) {
                    true -> {
                        trailerAdapter.setData(it.videos.videoResults.toCollection(ArrayList()))
                        rvTrailer.toVisible()
                        containerTrailerEmpty.toHide()
                    }
                    else -> {
                        rvTrailer.toHide()
                        containerTrailerEmpty.toVisible()
                    }
                }
                tvGenre.text = viewModel.generateGenre(it.genres, getString(R.string.lbl_genres))

                val url = it.homepage
                if (url.contains("www")) {
                    tvOfficial.apply {
                        toVisible()
                        text = url
                        setOnClickListener {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                        }
                    }
                }
            }
        }
    }

    companion object {
        private val TAG = DetailActivity::class.java.simpleName
        fun myIntent(context: Context, movie: MoviesResultResponse) =
            Intent(context, DetailActivity::class.java).apply {
                putExtra(EXTRA_MOVIE, movie)
            }

        private const val EXTRA_MOVIE = "EXTRA_MOVIE"
        const val YOUTUBE_WATCH_URL = "https://www.youtube.com/watch?v="
        private const val RELEASED = "RELEASED"
    }
}