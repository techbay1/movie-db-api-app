package com.flowerencee9.moviedbapiapps.screens.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.flowerencee9.moviedbapiapps.model.repository.DetailRepository
import com.flowerencee9.moviedbapiapps.model.response.Genre
import com.flowerencee9.moviedbapiapps.model.response.MovieDetailResponse
import com.flowerencee9.moviedbapiapps.model.response.ReviewResponse
import com.flowerencee9.moviedbapiapps.model.response.StatusResponse

class DetailViewModel(private val repository: DetailRepository, private val movieId: Int) :
    ViewModel() {
    val review: LiveData<PagingData<ReviewResponse>> =
        repository.getReview(movieId).cachedIn(viewModelScope)

    val loadingStates: LiveData<Boolean> get() = repository.loadingStates
    val statusResponse: LiveData<StatusResponse> get() = repository.statusResponse
    val detailMovie: LiveData<MovieDetailResponse> get() = repository.detailMovie

    fun getDetailMovie() = repository.getDetailMovie(movieId)

    fun generateGenre(list: List<Genre>, label: String): String {
        val result = StringBuilder()
        list.forEachIndexed { index, genre ->
            val insert = " ${genre.name},"
            if (index == list.size) insert.takeLast(1)
            result.append(insert)
        }
        return label + result.toString()
    }

}