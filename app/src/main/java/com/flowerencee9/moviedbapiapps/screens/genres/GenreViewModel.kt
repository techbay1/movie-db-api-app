package com.flowerencee9.moviedbapiapps.screens.genres

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.flowerencee9.moviedbapiapps.model.repository.MovieRepository
import com.flowerencee9.moviedbapiapps.model.response.Genre
import com.flowerencee9.moviedbapiapps.model.response.StatusResponse

class GenreViewModel(private val repository: MovieRepository) : ViewModel() {
    val loadingStates : LiveData<Boolean> get() = repository.loadingStates
    val statusResponse : LiveData<StatusResponse> get() = repository.statusResponse
    val listGenre : LiveData<ArrayList<Genre>> get() = repository.genreList

    fun getGenreList() = repository.getGenres()

}