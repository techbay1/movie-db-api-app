package com.flowerencee9.moviedbapiapps.screens.genres

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.flowerencee9.moviedbapiapps.R
import com.flowerencee9.moviedbapiapps.databinding.ActivityMainListBinding
import com.flowerencee9.moviedbapiapps.model.response.Genre
import com.flowerencee9.moviedbapiapps.networking.Injection
import com.flowerencee9.moviedbapiapps.screens.adapters.GenreAdapter
import com.flowerencee9.moviedbapiapps.screens.movie.MovieActivity
import com.flowerencee9.moviedbapiapps.support.PopUpCallback
import com.flowerencee9.moviedbapiapps.support.showPopupSelection
import com.flowerencee9.moviedbapiapps.support.toHide
import com.flowerencee9.moviedbapiapps.support.toVisible

class GenreActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainListBinding
    private lateinit var viewModel: GenreViewModel

    private val genreAdapter: GenreAdapter by lazy {
        GenreAdapter { genre: Genre ->
            genreClicked(genre)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainListBinding.inflate(layoutInflater)
        viewModel = GenreViewModel(Injection.provideMovie(this))
        setContentView(binding.root)
        setupView()
        initRequest()
    }

    override fun onResume() {
        super.onResume()
        viewModel.statusResponse.observe(this) {
            if (!it.success) {
                val selectionListener = object : PopUpCallback {
                    override fun onPrimary() {
                        initRequest()
                    }

                    override fun onSecondary() {
                        finish()
                    }

                }
                showPopupSelection(
                    null,
                    it.statusCode.toString(),
                    it.statusMessage,
                    getString(R.string.retry),
                    getString(R.string.close),
                    binding.root,
                    selectionListener
                )
            }
        }
    }

    private fun initRequest() {
        viewModel.getGenreList()
        viewModel.loadingStates.observe(this) {
            Log.d(TAG, "loading states $it")
        }
        viewModel.listGenre.observe(this) {
            Log.d(TAG, "list data genre $it")
            with(binding) {
                when (it.isNotEmpty()) {
                    true -> {
                        rvMain.toVisible()
                        containerMainEmpty.toHide()
                    }
                    else -> {
                        rvMain.toHide()
                        containerMainEmpty.toVisible()
                    }
                }
            }
            genreAdapter.setData(it)
        }
    }

    private fun setupView() {
        with(binding) {
            setSupportActionBar(toolbarMain)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            toolbarMain.title = getString(R.string.app_name)
            rvMain.apply {
                adapter = genreAdapter
                layoutManager = LinearLayoutManager(this@GenreActivity)
            }
        }
    }

    private fun genreClicked(genre: Genre) {
        Log.d(TAG, "genre clicked $genre")
        startActivity(MovieActivity.myIntent(this, genre))
    }

    companion object {
        private val TAG = GenreActivity::class.java.simpleName
    }
}