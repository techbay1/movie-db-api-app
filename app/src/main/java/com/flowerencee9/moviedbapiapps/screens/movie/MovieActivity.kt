package com.flowerencee9.moviedbapiapps.screens.movie

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.flowerencee9.moviedbapiapps.databinding.ActivityMainListBinding
import com.flowerencee9.moviedbapiapps.model.response.Genre
import com.flowerencee9.moviedbapiapps.model.response.MoviesResultResponse
import com.flowerencee9.moviedbapiapps.networking.Injection
import com.flowerencee9.moviedbapiapps.screens.adapters.LoadingStateAdapter
import com.flowerencee9.moviedbapiapps.screens.adapters.MovieAdapter
import com.flowerencee9.moviedbapiapps.screens.detail.DetailActivity
import com.flowerencee9.moviedbapiapps.support.toHide
import com.flowerencee9.moviedbapiapps.support.toVisible

class MovieActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainListBinding
    private lateinit var viewModel: MovieViewModel

    private val genre: Genre by lazy {
        intent.getParcelableExtra(EXTRA_GENRE) ?: Genre()
    }

    private val movieAdapter: MovieAdapter by lazy {
        MovieAdapter { moviesResultResponse ->
            movieClicked(moviesResultResponse)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainListBinding.inflate(layoutInflater)
        viewModel = MovieViewModel(Injection.provideMovie(this), genre.id.toString())
        setContentView(binding.root)
        initRequestData()
        setupView()
    }

    private fun movieClicked(movie: MoviesResultResponse) {
        Log.d(TAG, "clicked movie $movie")
        startActivity(DetailActivity.myIntent(this, movie))
    }

    private fun initRequestData() {
        viewModel.movies.observe(this) {
            movieAdapter.submitData(lifecycle, it)
        }
        movieAdapter.snapshot.observe(this) {
            Log.d(TAG, "list data $it")
            with(binding) {
                when (it.isNotEmpty()) {
                    true -> {
                        rvMain.toVisible()
                        containerMainEmpty.toHide()
                    }
                    else -> {
                        rvMain.toHide()
                        containerMainEmpty.toVisible()
                    }
                }
            }
        }
    }

    private fun setupView() {
        with(binding) {
            setSupportActionBar(toolbarMain)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            toolbarMain.title = "Discover ${genre.name}"
            rvMain.apply {
                adapter = movieAdapter.withLoadStateFooter(
                    footer = LoadingStateAdapter {
                        movieAdapter.retry()
                    }
                )
                layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            }
        }
    }

    companion object {
        private val TAG = MovieActivity::class.java.simpleName
        private const val EXTRA_GENRE = "EXTRA_GENRE"
        fun myIntent(context: Context, genre: Genre) =
            Intent(context, MovieActivity::class.java).apply {
                putExtra(EXTRA_GENRE, genre)
            }
    }
}